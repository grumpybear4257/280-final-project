﻿using System.Windows;
using UtilLibrary;

namespace ChatClient {
    /// <summary>
    /// Interaction logic for ClientScreenViewWindow.xaml
    /// </summary>
    public partial class ClientScreenViewWindow : Window {
        private readonly Client client;
        public readonly string nick;
        public bool WillClose { get; private set; }
        
        public ClientScreenViewWindow(Client client, string nick) {
            InitializeComponent();

            this.client = client;
            this.nick = nick;

            client.ReceivedScreenShot += Client_ReceivedScreenShot;
            client.DisconnectFromScreen += Client_DisconnectFromScreen;
        }

        private void Client_DisconnectFromScreen(string nick) {
            if (this.nick != nick && nick != "$__ALL__$")
                return;

            WillClose = true;
            Close();
        }

        private void Client_ReceivedScreenShot(SharedImage sharedImage) {
            if (sharedImage.Image == null) {
                WillClose = true;
                Close();

                return;
            }

            ClientScreenImg.Source = sharedImage.Image.ToSource();
            ClientScreenImg.InvalidateVisual();
        }
    }
}
