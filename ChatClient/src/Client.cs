﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using UtilLibrary;
using Message = UtilLibrary.Message;
using Size = System.Drawing.Size;

namespace ChatClient {
    public class Client {
        public delegate void ReceivedMessageHandler(Message message);
        public delegate void ReceivedPrivateMessageHandler(PrivateMessage message);
        public delegate void ReceivedRawMessageHandler(string message);
        public delegate void ReceivedScreenShotHandler(SharedImage sharedImage);
        public delegate void DisconnectFromScreenHandler(string nick);
        public event ReceivedMessageHandler ReceivedMessage;
        public event ReceivedPrivateMessageHandler ReceivedPrivateMessage;
        public event ReceivedRawMessageHandler ReceivedRawMessage;
        public event ReceivedScreenShotHandler ReceivedScreenShot;
        public event DisconnectFromScreenHandler DisconnectFromScreen;

        private readonly BackgroundWorker worker = new BackgroundWorker();
        private readonly IPAddress ipAddress;
        private readonly int port;

        private IFormatter formatter = new BinaryFormatter();
        private TcpClient chatClient, screenClient;
        private NetworkStream chatStream, screenStream;
        private BinaryWriter chatWriter, screenWriter;
        private BinaryReader chatReader;
        private bool screenSharing;

        public Client(IPAddress ipAddress, int port) {
            worker.WorkerSupportsCancellation = true;
            worker.WorkerReportsProgress = true;
            worker.DoWork += Worker_DoWork;
            worker.ProgressChanged += Worker_ProgressChanged;

            worker.RunWorkerAsync();

            this.ipAddress = ipAddress;
            this.port = port;
        }

        public string Nick { get; set; }

        public void SendMessage(string message) {
            message = message.Trim();
            if (message.ToLower() == "/stopshare") {
                if (screenClient == null) {
                    ReceivedRawMessage?.Invoke("You are not sharing your screen");
                    return;
                }

                screenClient?.Close();
                screenSharing = false;

                ReceivedRawMessage?.Invoke("You are no longer sharing your screen");
            }

            try {
                formatter.Serialize(chatWriter.BaseStream, message);
            } catch (Exception ex) {
                Console.WriteLine(ex);
                throw;
            }
        }

        public void Disconnect() {
            DisconnectFromScreen?.Invoke("$__ALL__$");
            if (screenSharing)
                formatter.Serialize(screenWriter.BaseStream, new SharedImage(Nick, null));

            worker.CancelAsync();

            screenClient?.Close();
            chatClient.Close();

            formatter = null;
            screenSharing = false;
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e) {
            try {
                chatClient = new TcpClient();
                chatClient.Connect(ipAddress, port);

                chatStream = chatClient.GetStream();
                chatReader = new BinaryReader(chatStream);
                chatWriter = new BinaryWriter(chatStream);

                worker.ReportProgress(3, $"CONNECTED TO - {ipAddress}:{port}");
            } catch (Exception) {
                worker.ReportProgress(-1);
                return;
            }

            try {
                while (worker.CancellationPending == false) {
                    object data = formatter.Deserialize(chatStream);
                    if (data is Message message) {
                        if (message.Sender == "SRVCMD") {
                            // It's a server command, decode it
                            string[] command = message.Content.Split(':');
                            string[] parameters = command[1].Split('_');
                            switch (command[0]) {
                                case "CONSCRN":
                                    IPAddress address = IPAddress.Parse(parameters[0]);
                                    if (int.TryParse(parameters[1], out int sp) == false)
                                        continue;
                                    if (int.TryParse(parameters[2], out int fps) == false)
                                        continue;

                                    // Open a connection to the server, and start send the screen data
                                    HandleScreenShare(new IPEndPoint(address, sp), fps);
                                    screenSharing = true;

                                    worker.ReportProgress(3,
                                        "Now sharing screen with server. Type /stopshare to stop sharing.");

                                    continue;
                                case "DCFRMSCRN": {
                                        worker.ReportProgress(2, parameters[0]);
                                        continue;
                                    }
                                case "NICK": {
                                        Nick = parameters[0];
                                        continue;
                                    }
                                default:
                                    continue;
                            }
                        }

                        worker.ReportProgress(0, message);
                    } else if (data is PrivateMessage privateMessage) {
                        worker.ReportProgress(4, privateMessage);
                    } else if (data is string rawMessage) {
                        worker.ReportProgress(3, rawMessage);
                    } else if (data is SharedImage image) {
                        worker.ReportProgress(1, image);
                    }
                }

                chatClient.Close();
            } catch (Exception ex) {
                Console.WriteLine(ex);
                worker.ReportProgress(99);
            } finally {
                chatWriter.Close();
                chatReader.Close();
                chatStream.Close();
                chatClient.Close();
                screenWriter?.Close();
                screenStream?.Close();
                screenClient?.Close();
                worker.CancelAsync();
            }
        }

        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            if (e.ProgressPercentage == 99)
                ReceivedRawMessage?.Invoke("Disconnected");
            else if (e.ProgressPercentage == -1)
                ReceivedRawMessage?.Invoke("Unable to connect to server!");
            else if (e.ProgressPercentage == 0)
                ReceivedMessage?.Invoke(e.UserState as Message);
            else if (e.ProgressPercentage == 4)
                ReceivedPrivateMessage?.Invoke(e.UserState as PrivateMessage);
            else if (e.ProgressPercentage == 3)
                ReceivedRawMessage?.Invoke(e.UserState as string);
            else if (e.ProgressPercentage == 1)
                ReceivedScreenShot?.Invoke(e.UserState as SharedImage);
            else if (e.ProgressPercentage == 2)
                DisconnectFromScreen?.Invoke(e.UserState as string);
        }

        [DllImport("gdi32.dll")]
        private static extern bool DeleteObject(IntPtr hObject);

        private void HandleScreenShare(IPEndPoint endPoint, int fps) {
            Task.Run(() => {
                screenClient = new TcpClient();
                screenClient.Connect(endPoint);

                screenStream = screenClient.GetStream();
                screenWriter = new BinaryWriter(screenStream);

                int width = Screen.PrimaryScreen.Bounds.Width;
                int height = Screen.PrimaryScreen.Bounds.Height;

                TimeSpan sleepTime = GetInterval(fps);

                Bitmap screenBmp = new Bitmap(width, height, PixelFormat.Format32bppArgb);
                Graphics bmpGraphics = Graphics.FromImage(screenBmp);

                SendMessage("/startedsharing");

                while (screenSharing && screenClient.Connected) {
                    Stopwatch sw = Stopwatch.StartNew();
                    try {
                        bmpGraphics.CopyFromScreen(
                            Screen.PrimaryScreen.Bounds.X,
                            Screen.PrimaryScreen.Bounds.Y,
                            0, 0,
                            new Size(width, height)
                        );

                        IntPtr intPtr = screenBmp.GetHbitmap();
                        formatter.Serialize(screenWriter.BaseStream, new SharedImage(Nick, Image.FromHbitmap(intPtr)));
                        screenWriter.Flush();

                        // https://stackoverflow.com/a/1714915/7754335
                        DeleteObject(intPtr);

                        sw.Stop();
                        TimeSpan temp = sleepTime - sw.Elapsed;
                        if (temp > TimeSpan.Zero)
                            Thread.Sleep(temp);
                    } catch (Exception ex) {
                        if (ex.Message.Contains("WSACancelBlockingCall")) {

                        }
                    }
                }

                screenBmp.Dispose();
                bmpGraphics.Dispose();
            });
        }

        private TimeSpan GetInterval(int fps) {
            return TimeSpan.FromMilliseconds(1000 / fps);
        }
    }
}
