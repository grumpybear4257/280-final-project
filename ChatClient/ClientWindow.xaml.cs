﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Windows;
using System.Windows.Input;
using Notifications.Wpf;
using UtilLibrary;
using Application = System.Windows.Application;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using Message = UtilLibrary.Message;

namespace ChatClient {
    /// <summary>
    /// Interaction logic for ClientWindow.xaml
    /// </summary>
    public partial class ClientWindow : Window {
        private Client client;
        private bool connected;
        private string lastSentMessage = string.Empty;
        private readonly IPAddress myIp = Dns.GetHostEntry(Dns.GetHostName()).AddressList
            .First(ip => ip.AddressFamily == AddressFamily.InterNetwork);
        private readonly NotificationManager nm = new NotificationManager();
        private readonly List<Tuple<ClientScreenViewWindow, string>> viewWindows = new List<Tuple<ClientScreenViewWindow, string>>();

        public ClientWindow() {
            InitializeComponent();

            // Set defaults for the server ip and port fields (default to localhost)
            ServerTxt.Text = myIp.ToString();
            PortTxt.Text = 5000.ToString();
        }

        private void ConnectBtn_OnClick(object sender, RoutedEventArgs e) {
            if (connected) {
                // Disconnect from the server
                ConnectBtn.Content = "Connect";

                client.Disconnect();
                connected = false;
                client = null;
            } else {
                // Connect to the sever
                MessagesLsbx.Items.Clear();
                if (IPAddress.TryParse(ServerTxt.Text, out IPAddress ip)) {
                    if (ip.AddressFamily == AddressFamily.InterNetwork) {
                        if (int.TryParse(PortTxt.Text, out int port)) {
                            client = new Client(ip, port);
                            client.ReceivedMessage += Client_ReceivedMessage;
                            client.ReceivedRawMessage += Client_ReceivedRawMessage;
                            client.ReceivedScreenShot += Client_ReceivedScreenShot;
                            client.ReceivedPrivateMessage += Client_ReceivedPrivateMessage;
                            connected = true;

                            ConnectBtn.Content = "Disconnect";

                            return;
                        }

                        MessagesLsbx.Items.Add("Please enter a valid port!");
                        return;
                    }
                }

                MessagesLsbx.Items.Add("Please enter a valid IPv4 address!");
            }
        }

        private void Client_ReceivedScreenShot(SharedImage sharedImage) {
            bool windowAlreadyOpen = viewWindows.FirstOrDefault(entry => entry.Item2 == sharedImage.Sender) != null;
            if (windowAlreadyOpen == false) {
                ClientScreenViewWindow viewWindow = new ClientScreenViewWindow(client, sharedImage.Sender);
                viewWindow.Show();

                viewWindow.Closed += ViewWindow_Closed;

                viewWindows.Add(new Tuple<ClientScreenViewWindow, string>(viewWindow, sharedImage.Sender));
            }
        }

        private void ViewWindow_Closed(object sender, EventArgs e) {
            ClientScreenViewWindow window = (ClientScreenViewWindow)sender;

            Tuple<ClientScreenViewWindow, string> temp = viewWindows.FirstOrDefault(entry => entry.Item1 == window);
            if (temp == null)
                return;

            viewWindows.Remove(temp);
            if (window.WillClose == false)
                client?.SendMessage($"/disconshare {window.nick}");
        }

        private void Client_ReceivedRawMessage(string message) {
            if (message == "Unable to connect to server!" || message == "Disconnected") {
                ConnectBtn.Content = "Connect";
                connected = false;
                client = null;

                try {
                    foreach ((ClientScreenViewWindow viewWindow, string _) in viewWindows)
                        viewWindow.Close();
                } catch (Exception) {
                    //ignored
                }
            }

            MessagesLsbx.Items.Add(message);
        }

        private void Client_ReceivedMessage(Message message) {
            string formattedMessage = $"{message.SentDate:h:mm:ss tt} - {message.Sender}: {message.Content}";
            MessagesLsbx.Items.Add(formattedMessage);
            MessagesLsbx.ScrollIntoView(formattedMessage);

            if (message.Sender == client.Nick)
                return;

            nm.Show(new NotificationContent {
                Title = $"New Message From {message.Sender}",
                Message = message.Content,
                Type = NotificationType.Information
            });
        }

        private void Client_ReceivedPrivateMessage(PrivateMessage message) {
            if (message.Sender != client.Nick && message.Receiver != client.Nick)
                // how did we even get this? The server fucked up 
                return;

            string sender = message.Sender == client.Nick ? "Me" : message.Sender;
            string receiver = message.Receiver == client.Nick ? "Me" : message.Receiver;
            string formattedMessage = $"{message.SentDate:h:mm:ss tt} - {sender} -> {receiver}: {message.Content}";

            MessagesLsbx.Items.Add(formattedMessage);
            MessagesLsbx.ScrollIntoView(formattedMessage);

            if (message.Sender != client.Nick)
                return;

            nm.Show(new NotificationContent {
                Title = $"New Private Message From {message.Sender}",
                Message = message.Content,
                Type = NotificationType.Success
            });
        }

        private void PortTxt_OnPreviewTextInput(object sender, TextCompositionEventArgs e) {
            if (int.TryParse(e.Text, out int _) == false)
                e.Handled = true;
        }

        private void PortTxt_OnPasting(object sender, DataObjectPastingEventArgs e) {
            if (e.DataObject.GetDataPresent(typeof(string))) {
                string text = e.DataObject.GetData(typeof(string)) as string;
                if (int.TryParse(text, out int _) == false)
                    e.CancelCommand();
            } else {
                e.CancelCommand();
            }
        }

        private void SendBtn_Click(object sender, RoutedEventArgs e) {
            try {
                string input = MessageTxt.Text.Trim();
                client.SendMessage(input);

                lastSentMessage = input;
            } catch (NullReferenceException) {
                MessagesLsbx.Items.Add("You aren't connected to a server!");
            } finally {
                MessageTxt.Clear();
            }
        }

        private void MessageTxt_OnKeyUp(object sender, KeyEventArgs e) {
            switch (e.Key) {
                // Check to see if they hit the up arrow key, or the page up key, if they did, put the last message they sent in the textbox
                case Key.Up:
                case Key.PageUp:
                    // If we wanted to go even further, we could have a list of all messages sent, and cycle through that,
                    // but I don't really see a point in going that far.
                    MessageTxt.Text = lastSentMessage;
                    MessageTxt.SelectionStart = MessageTxt.Text.Length;
                    break;
                case Key.Down:
                case Key.PageDown:
                    // Set the textbox to empty 
                    MessageTxt.Text = string.Empty;
                    break;
            }
        }

        private void ClientWindow_OnClosing(object sender, CancelEventArgs e) {
            client?.Disconnect();
            Application.Current.Shutdown();
        }

        private void ClearBtn_OnClick(object sender, RoutedEventArgs e) {
            MessagesLsbx.Items.Clear();
        }
    }
}