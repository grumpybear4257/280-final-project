﻿using System;
using System.Drawing;

namespace UtilLibrary {
    [Serializable]
    public class SharedImage {
        public readonly string Sender;
        public readonly Bitmap Image;

        public SharedImage(string sender, Bitmap image) {
            Sender = sender;
            Image = image;
        }
    }
}
