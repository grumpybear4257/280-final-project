﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UtilLibrary {
    public static class GeneralUtils {
        public static string GetString(this IEnumerable<string> list, char separator) {
            return list.Aggregate(string.Empty, (current, item) => current + $"{item}{separator}").TrimEnd(separator);
        }

        public static string Format(this TimeSpan timeSpan) {
            string days, hours;
            if (timeSpan.Days > 1)
                days = $"{timeSpan.Days} days";
            else if (timeSpan.Days == 0)
                days = string.Empty;
            else
                days = $"{timeSpan.Days} day";

            if (timeSpan.Hours > 1)
                hours = $"{timeSpan.Hours} hours";
            else if (timeSpan.Days == 0 && timeSpan.Hours == 0)
                hours = string.Empty;
            else
                hours = $"{timeSpan.Hours} hour";

            string minutes = timeSpan.Minutes > 1 ? $"{timeSpan.Minutes} minutes" : $"{timeSpan.Minutes} minute";
            string seconds = timeSpan.Seconds > 1 ? $"{timeSpan.Seconds} seconds" : $"{timeSpan.Seconds} second";

            if (string.IsNullOrEmpty(days) && string.IsNullOrEmpty(hours) == false)
                return $"{hours}, {minutes}, {seconds}";

            if (string.IsNullOrEmpty(hours))
                return $"{minutes}, {seconds}";

            return $"{days}, {hours}, {minutes}, {seconds}";
        }
    }
}
