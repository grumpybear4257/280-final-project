﻿using System;

namespace UtilLibrary {
    [Serializable]
    public class Message {
        public readonly string Sender;
        public readonly string Content;
        public readonly DateTime SentDate;

        public Message(string sender, string content) {
            Sender = sender;
            Content = content;
            SentDate = DateTime.Now;
        }
    }

    [Serializable]
    public class PrivateMessage {
        public readonly string Sender;
        public readonly string Receiver;
        public readonly string Content;
        public readonly DateTime SentDate;

        public PrivateMessage(string sender, string receiver, string content) {
            Sender = sender;
            Receiver = receiver;
            Content = content;
            SentDate = DateTime.Now;
        }
    }
}
