﻿using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using PixelFormat = System.Drawing.Imaging.PixelFormat;
using Point = System.Drawing.Point;

namespace UtilLibrary {
    public static class BitmapUtils {
        public static ImageSource ToSource(this Bitmap image) {
            BitmapData bitmapData = image.LockBits(new Rectangle(Point.Empty, image.Size), ImageLockMode.ReadOnly, PixelFormat.Format32bppPArgb);
            BitmapSource bitmapSource = BitmapSource.Create(bitmapData.Width, bitmapData.Height,
                image.HorizontalResolution, image.VerticalResolution, PixelFormats.Pbgra32, null, bitmapData.Scan0,
                bitmapData.Stride * bitmapData.Height, bitmapData.Stride);

            image.UnlockBits(bitmapData);
            return bitmapSource;
        }
    }
}
