﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using UtilLibrary;

namespace ChatServer {
    public enum ClientState {
        Connected,
        Disconnected,
        NameChange
    }

    public class Server {
        public delegate void ReceivedMessageHandler(Message message);
        public delegate void ReceivedScreenShotHandler(TcpClient client, SharedImage image);
        public delegate void ChatClientStateChange(string nick, ClientState state, string oldNick = "");
        public delegate void ScreenShareClientStateChange(TcpClient client, ClientState state);
        public event ReceivedMessageHandler ReceivedMessage;
        public event ReceivedScreenShotHandler ReceivedScreenShot;
        public event ChatClientStateChange ChatClientStateChanged;
        public event ScreenShareClientStateChange ScreenShareClientStateChanged;

        public DateTime ServerStart { get; private set; }
        public const string ServerNick = "SERVER";
        public const string ServerCmd = "SRVCMD";

        private readonly Dictionary<TcpClient, string> clientNicknames = new Dictionary<TcpClient, string>();
        private readonly List<Tuple<string, List<string>>> clientsGettingScreens = new List<Tuple<string, List<string>>>(); // A list of tcp clients that are sharing their screen, and to who
        private readonly List<Tuple<string, DateTime>> mutedClients = new List<Tuple<string, DateTime>>();
        private readonly Dictionary<TcpClient, ConcurrentQueue<object>> clientMessageQueues = new Dictionary<TcpClient, ConcurrentQueue<object>>();
        private readonly BackgroundWorker screenWorker = new BackgroundWorker();
        private readonly BackgroundWorker chatWorker = new BackgroundWorker();
        private readonly IFormatter formatter = new BinaryFormatter();
        private readonly TcpListener screenListener;
        private readonly TcpListener chatListener;

        // Format is item1 = command name, item2 = command description, item3 = command usage
        private static readonly List<Tuple<string, string, string>> Commands = new List<Tuple<string, string, string>>();

        static Server() {
            // Initialize commands
            Commands.Add(new Tuple<string, string, string>("Help", "View a list of all commands (You are here!)", "/help"));
            Commands.Add(new Tuple<string, string, string>("Nick", "Change your nickname", "/nick {nickname}"));
            Commands.Add(new Tuple<string, string, string>("List", "View a list of all currently online clients", "/list"));
            Commands.Add(new Tuple<string, string, string>("Uptime", "Shows the amount of time the server has been running", "/uptime"));
            Commands.Add(new Tuple<string, string, string>("Message", "Send a message to only the specified user", "/msg {receiver nickname} {message}"));
            Commands.Add(new Tuple<string, string, string>("Shares", "List all the users currently sharing their screen", "/shares"));
            Commands.Add(new Tuple<string, string, string>("StartShare", "Start sharing your screen with the server", "/startshare"));
            Commands.Add(new Tuple<string, string, string>("StopShare", "Stop sharing your screen with the server", "/stopshare"));
            Commands.Add(new Tuple<string, string, string>("ConShare", "Connects to a users screen share", "/conshare {user nickname}"));
            Commands.Add(new Tuple<string, string, string>("DisConShare", "Disconnect from a users screen share", "/disconshare {user nickname}"));
        }

        public Server(TcpListener screenListener, TcpListener chatListener) {
            this.screenListener = screenListener;
            this.chatListener = chatListener;

            screenWorker.WorkerSupportsCancellation = true;
            screenWorker.WorkerReportsProgress = true;
            chatWorker.WorkerSupportsCancellation = true;
            screenWorker.DoWork += ScreenWorker_DoWork;
            chatWorker.DoWork += ChatWorker_DoWork;
            screenWorker.ProgressChanged += ScreenWorker_ProgressChanged;
        }

        private void ScreenWorker_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            if (!(e.UserState is Tuple<TcpClient, SharedImage> data))
                return;

            ReceivedScreenShot?.Invoke(data.Item1, data.Item2);

            List<string> receivers = clientsGettingScreens.Find(entry => entry.Item1 == data.Item2.Sender)?.Item2;
            if (receivers == null)
                return;

            lock (receivers) {
                foreach (string receiver in receivers) {
                    TcpClient client = GetClientFromNick(receiver);
                    if (client != null)
                        clientMessageQueues[client].Enqueue(data.Item2);
                }
            }
        }

        public void Start() {
            chatWorker.RunWorkerAsync();
            screenWorker.RunWorkerAsync();
            ChatClientStateChanged?.Invoke(ServerNick, ClientState.Connected);

            ServerStart = DateTime.Now;
        }

        public void Stop() {
            SendMessageFromServer("Stopping server!");
            ChatClientStateChanged?.Invoke(ServerNick, ClientState.Disconnected);

            foreach (KeyValuePair<TcpClient, string> pair in clientNicknames)
                pair.Key.Client.Close();

            screenWorker.CancelAsync();
            chatWorker.CancelAsync();
            screenListener.Stop();
            chatListener.Stop();

            ServerStart = DateTime.MinValue;
        }

        /// <summary>
        /// Send a message from the server, to everyone connected to the server
        /// </summary>
        /// <param name="message"></param>
        public void SendMessageFromServer(string message) {
            if (message.StartsWith("/")) {
                string[] rawCommand = message.Remove(0, 1).Trim().Split(' ');
                string command = rawCommand[0].ToLower();
                string[] parameters = rawCommand.Skip(1).ToArray();

                switch (command) {
                    case "kick": {
                            // Make sure they entered someone to kick
                            if (parameters.Length == 0) {
                                ReceivedMessage?.Invoke(new Message("Syntax Error", "Please enter a nickname to kick! (usage: '/kick {nickname}')"));
                                break;
                            }

                            // Go through all the nicks they entered (eg '/kick Nathan Nigel' will kick both Nathan and Nigel) and validate them
                            foreach (string nick in parameters) {
                                if (clientNicknames.ContainsValue(nick)) {
                                    TcpClient clientToKick = clientNicknames.First(client => client.Value == nick).Key;
                                    clientMessageQueues[clientToKick].Enqueue(new Message(ServerNick, "You have been kicked from the server!"));
                                    
                                    // Sleep so that they have time to get the message.
                                    Thread.Sleep(100);

                                    clientToKick.Client.Close();
                                    // Make sure they no longer get screenshares
                                    foreach (Tuple<string, List<string>> tuple in clientsGettingScreens)
                                        tuple.Item2.Remove(nick);

                                    SendMessageFromServer($"{nick} was kicked from the server!");
                                } else {
                                    // Inform invoker (server) that the nick they entered wasn't found
                                    ReceivedMessage?.Invoke(new Message("Invalid Parameter", $"Unable to find client with nickname {nick}!"));
                                }
                            }

                            break;
                        }
                    case "mute": {
                            // Make sure the entered someone to mute
                            if (parameters.Length == 0) {
                                ReceivedMessage?.Invoke(new Message("Syntax Error", "Please enter a nickname to mute (usage: '/mute {nickname} [duration in minutes, default = 5]')"));
                                break;
                            }

                            string nick = parameters[0];
                            int durationMinutes = 5;

                            // Check to see if they specified a duration 
                            if (parameters.Length == 2 && int.TryParse(parameters[1], out durationMinutes) == false) {
                                ReceivedMessage?.Invoke(new Message("Invalid Parameter", $"Cannot parse duration ({parameters[1]}) to int"));
                                break;
                            }

                            if (mutedClients.Any(client => client.Item1 == nick)) {
                                ReceivedMessage?.Invoke(new Message("Invalid Operation", $"{nick} is already muted!"));
                                break;
                            }

                            if (clientNicknames.ContainsValue(nick)) {
                                SendMessageFromServer($"{nick} is now muted for {durationMinutes} minutes!");
                                mutedClients.Add(new Tuple<string, DateTime>(nick, DateTime.Now.AddMinutes(durationMinutes)));

                                break;
                            }

                            ReceivedMessage?.Invoke(new Message("Invalid parameter", $"Unable to find client with nickname {nick}!"));
                            break;
                        }
                    case "unmute": {
                            if (parameters.Length == 0) {
                                ReceivedMessage?.Invoke(new Message("Syntax Error", "Please enter a nickname to unmute (usage: '/unmute {nickname}"));
                                break;
                            }

                            string nick = parameters[0];
                            if (clientNicknames.ContainsValue(nick) == false) {
                                ReceivedMessage?.Invoke(new Message("Invalid Parameter", $"Unable to find client with nickname {nick}!"));
                                break;
                            }

                            Tuple<string, DateTime> mutedClient = mutedClients.FirstOrDefault(client => client.Item1 == nick);
                            if (mutedClient != null) {
                                mutedClients.Add(new Tuple<string, DateTime>(mutedClient.Item1, DateTime.Now));
                                mutedClients.Remove(mutedClient);
                                break;
                            }

                            ReceivedMessage?.Invoke(new Message("Invalid Operation", $"{nick} isn't muted!"));
                            break;
                        }
                    default: {
                            ReceivedMessage?.Invoke(new Message("Unknown Command", $"{command}"));
                            break;
                        }
                }

                return;
            }

            HandleNewMessage(new Message(ServerNick, message));
        }

        public void ViewScreen(string nick) {
            // Check to see if they are already sharing
            if (clientsGettingScreens.Find(item => item.Item1 == nick) != null) {
                ReceivedMessage?.Invoke(new Message("SCREEN::INFO", $"{nick} is already sharing their screen!"));
                return;
            }

            // Tell them to start sharing
            IPEndPoint endPoint = (IPEndPoint)screenListener.LocalEndpoint;
            clientMessageQueues[GetClientFromNick(nick)].Enqueue(new Message(ServerCmd, $"CONSCRN:{endPoint.Address}_{endPoint.Port}_10"));
            ReceivedMessage?.Invoke(new Message("SCREEN::INFO", $"Attempting to connect to {nick} for screen share session."));
            // Hope that they listen to our command.
        }

        private void ScreenWorker_DoWork(object sender, DoWorkEventArgs e) {
            while (screenWorker.CancellationPending == false) {
                try {
                    TcpClient client = screenListener.AcceptTcpClient();
                    Task.Run(() => HandleScreenShareClient(client));
                } catch (Exception ex) {
                    if (ex.Message.Contains("WSACancelBlockingCall"))
                        return;

                    ReceivedMessage?.Invoke(new Message("MAIN::ERROR", ex.Message));
                }
            }
        }

        private void ChatWorker_DoWork(object sender, DoWorkEventArgs e) {
            // Start a thread that will manage muted clients
            Task.Run(() => HandleMutedClients());

            // Start accepting connections
            while (chatWorker.CancellationPending == false) {
                try {
                    TcpClient connection = chatListener.AcceptTcpClient();

#pragma warning disable 4014 // we don't want to wait for this method to finish, the whole point of it is to NOT finish until the client disconnects
                    Task.Run(() => HandleChatClient(connection));
#pragma warning restore 4014
                } catch (Exception ex) {
                    if (ex.Message.Contains("WSACancelBlockingCall"))
                        return;

                    ReceivedMessage?.Invoke(new Message("MAIN::ERROR", ex.Message));
                }
            }
        }

        private void HandleMutedClients() {
            while (chatWorker.CancellationPending == false) {
                // Only check once per second
                Thread.Sleep(1000);

                List<Tuple<string, DateTime>> toRemove = new List<Tuple<string, DateTime>>();

                foreach (Tuple<string, DateTime> mutedClient in mutedClients) {
                    if (mutedClient.Item2 > DateTime.Now)
                        continue;

                    // is the user even online still?
                    BinaryWriter writer = GetWriterFromNick(mutedClient.Item1);
                    if (writer == null)
                        continue;

                    SendMessageFromServer($"{mutedClient.Item1} is no longer muted");
                    toRemove.Add(mutedClient);
                }

                foreach (Tuple<string, DateTime> tuple in toRemove)
                    mutedClients.Remove(tuple);
            }
        }

        private string GetNickName(TcpClient client) {
            NetworkStream stream = client.GetStream();
            BinaryWriter writer = new BinaryWriter(stream);
            BinaryReader reader = new BinaryReader(stream);

            string nick = string.Empty;
            while (string.IsNullOrWhiteSpace(nick)) {
                try {
                    formatter.Serialize(writer.BaseStream, "Please enter a nickname");
                    nick = formatter.Deserialize(new BinaryReader(stream).BaseStream).ToString().Trim();

                    if (ValidateNick(writer, ref nick) == false) {
                        nick = string.Empty;
                        continue;
                    }

                    formatter.Serialize(writer.BaseStream, $"Your nickname will be: '{nick}'. Is that OK? (y/n)");

                    string confirm = formatter.Deserialize(reader.BaseStream).ToString().Trim();
                    if (confirm != "y" && confirm != "yes")
                        nick = string.Empty;
                } catch (Exception) {
                    // Do nothing
                    return string.Empty;
                }
            }

            clientMessageQueues.Add(client, new ConcurrentQueue<object>());

            formatter.Serialize(writer.BaseStream, $"Welcome to the server, {nick}! Do /help for a list of commands!");
            clientMessageQueues[client].Enqueue(new Message(ServerCmd, $"NICK:{nick}"));
            
            return nick;
        }

        private void HandleScreenShareClient(TcpClient client) {
            NetworkStream socketStream = client.GetStream();
            BinaryReader reader = new BinaryReader(socketStream);

            ScreenShareClientStateChanged?.Invoke(client, ClientState.Connected);

            while (screenWorker.CancellationPending == false && client.Connected) {
                try {
                    object data = formatter.Deserialize(reader.BaseStream);
                    if (!(data is SharedImage image))
                        continue;

                    screenWorker.ReportProgress(1, new Tuple<TcpClient, SharedImage>(client, image));
                } catch (Exception) {
                    client.Close();
                    client.Dispose();
                    ScreenShareClientStateChanged?.Invoke(client, ClientState.Disconnected);
                }
            }
        }

        private void KeepClientUpdated(TcpClient client, ref bool connected) {
            while (connected) {
                ConcurrentQueue<object> queue = clientMessageQueues[client];
                if (queue.IsEmpty) {
                    Thread.Sleep(50);
                    continue;
                }

                if (queue.TryDequeue(out object result)) { 
                    try {
                        lock (result) {
                            BinaryWriter writer = new BinaryWriter(client.GetStream());
                            if (result is SharedImage image)
                                formatter.Serialize(writer.BaseStream, new SharedImage(image.Sender, image.Image));
                            else
                                formatter.Serialize(writer.BaseStream, result);
                        }
                    } catch (Exception) {
                        // Hm, probably can ignore?
                    }
                }

                if (queue.Count <= 100)
                    continue;

                IEnumerable<object> temp = (from queueItem in queue where queueItem is SharedImage == false select queueItem);
                while (clientMessageQueues[client].IsEmpty == false) {
                    if (clientMessageQueues[client].TryDequeue(out object _)) {
                    }
                }

                clientMessageQueues[client] = new ConcurrentQueue<object>(temp);
            }
        }

        private async void HandleChatClient(TcpClient client) {
            Task<string> getNickTask = Task.Run(() => GetNickName(client));
            string nick = await getNickTask;

            if (string.IsNullOrWhiteSpace(nick))
                return;

            clientNicknames.Add(client, nick);
            SendMessageFromServer($"{nick} has joined the chat.");
            ChatClientStateChanged?.Invoke(nick, ClientState.Connected);

            NetworkStream socketStream = client.GetStream();
            BinaryWriter writer = new BinaryWriter(socketStream);
            BinaryReader reader = new BinaryReader(socketStream);

            bool connected = true;

#pragma warning disable 4014
            Task.Run(() => KeepClientUpdated(client, ref connected));
#pragma warning restore 4014

            while (chatWorker.CancellationPending == false && client.Connected) {
                try {
                    object data = formatter.Deserialize(reader.BaseStream);
                    nick = clientNicknames[client]; // Update this so that if they change their nick, it actually shows the change

                    // Could also check for other types, like a file
                    if (!(data is string message))
                        continue;

                    if (mutedClients.Any(mc => mc.Item1 == nick)) {
                        clientMessageQueues[client].Enqueue(new Message(ServerNick, "You are muted!"));
                        continue;
                    }

                    // Handle commands 
                    if (message.StartsWith("/")) {
                        string[] rawCommand = message.Remove(0, 1).Trim().Split(' ');
                        string command = rawCommand[0].ToLower();
                        string[] parameters = rawCommand.Skip(1).ToArray();

                        switch (command) {
                            case "shares": {
                                    string clients = (from tuple in clientsGettingScreens select tuple.Item1).GetString('\n');
                                    clients = string.IsNullOrWhiteSpace(clients) ? "None" : clients;
                                    clientMessageQueues[client].Enqueue($"Clients currently sharing their screen:\n{clients}");

                                    continue;
                                }
                            case "startshare": {
                                    ViewScreen(nick);
                                    continue;
                                }
                            case "startedsharing": {
                                    SendMessageFromServer($"{nick} is now sharing their screen. Type '/conshare {nick}' to connect.");
                                    clientsGettingScreens.Add(new Tuple<string, List<string>>(nick, new List<string> { ServerNick }));
                                    continue;
                                }
                            case "conshare": {
                                    if (parameters.Length == 0) {
                                        clientMessageQueues[client].Enqueue("Please specify a user to connect to.");
                                        continue;
                                    }

                                    string targetNick = parameters[0];
                                    if (targetNick == nick) {
                                        clientMessageQueues[client].Enqueue("You cannot view your own screen remotely!");
                                        continue;
                                    }

                                    TcpClient targetClient = GetClientFromNick(targetNick);
                                    if (targetClient == null) {
                                        clientMessageQueues[client].Enqueue($"Cannot find {targetNick}, are they online?");
                                        continue;
                                    }

                                    Tuple<string, List<string>> temp = clientsGettingScreens.FirstOrDefault(who => who.Item1 == targetNick);
                                    if (temp == null) {
                                        clientMessageQueues[client].Enqueue($"{targetNick} does not appear to be sharing their screen.");
                                        continue;
                                    }

                                    List<string> nicks = temp.Item2;
                                    nicks.Add(nick);

                                    clientMessageQueues[targetClient].Enqueue(new Message(ServerNick, $"{nick} is now viewing your screen"));
                                    clientMessageQueues[client].Enqueue($"You are now connected to {targetNick}'s screenshare.");

                                    continue;
                                }
                            case "disconshare": {
                                    if (parameters.Length == 0) {
                                        clientMessageQueues[client].Enqueue("Please specify a user to disconnect from.");
                                        continue;
                                    }

                                    string targetNick = parameters[0];
                                    if (targetNick == nick) {
                                        clientMessageQueues[client].Enqueue("You cannot disconnect from yourself");
                                        continue;
                                    }

                                    Tuple<string, List<string>> temp = clientsGettingScreens.FirstOrDefault(sharer => sharer.Item1 == targetNick);
                                    if (temp == null) {
                                        clientMessageQueues[client].Enqueue($"{targetNick} is not sharing their screen. Therefore, you are not connected to them");
                                        continue;
                                    }

                                    if (temp.Item2.Contains(nick) == false) {
                                        clientMessageQueues[client].Enqueue($"You are not connected to {targetNick}");
                                        continue;
                                    }

                                    lock (temp.Item2)
                                        temp.Item2.Remove(nick);

                                    // Don't know how useful this is...
                                    ConcurrentQueue<object> queue = clientMessageQueues[client];
                                    List<object> itemsToKeep = new List<object>();
                                    foreach (object item in queue) {
                                        if (!(item is SharedImage image) || image.Sender != targetNick)
                                            itemsToKeep.Add(item);
                                    }

                                    clientMessageQueues[client] = new ConcurrentQueue<object>(itemsToKeep);

                                    clientMessageQueues[GetClientFromNick(targetNick)].Enqueue(new Message(ServerNick, $"{nick} is no longer viewing your screen"));
                                    clientMessageQueues[client].Enqueue(new Message(ServerCmd, $"DCFRMSCRN:{targetNick}"));
                                    clientMessageQueues[client].Enqueue($"You are no longer connected to {targetNick}'s screenshare.");

                                    continue;
                                }
                            case "stopshare": {
                                    Tuple<string, List<string>> tuple = clientsGettingScreens.Find(sharer => sharer.Item1 == nick);
                                    if (tuple == null) {
                                        clientMessageQueues[client].Enqueue("You do not appear to be sharing your screen!");
                                        continue;
                                    }

                                    foreach (string receivingClients in tuple.Item2)
                                        if (receivingClients != ServerNick)
                                            clientMessageQueues[GetClientFromNick(receivingClients)].Enqueue(new Message(ServerCmd, $"DCFRMSCRN:{nick}"));

                                    clientsGettingScreens.Remove(tuple);
                                    SendMessageFromServer($"{nick} is no longer sharing their screen.");
                                    continue;
                                }
                            case "nick": {
                                    if (parameters.Length == 0) {
                                        clientMessageQueues[client].Enqueue("Please enter a nickname! (usage: '/nick {nickname}')");

                                        continue;
                                    }

                                    if (parameters.Length > 1) {
                                        clientMessageQueues[client].Enqueue("Nicknames can only be one word!");
                                        continue;
                                    }

                                    ChangeNick(parameters[0].Trim(), writer, client);

                                    continue;
                                }
                            case "help": {
                                    clientMessageQueues[client].Enqueue("The commands available on this server are:");
                                    foreach ((string name, string description, string usage) in Commands)
                                        clientMessageQueues[client].Enqueue($"{name}: {description}\n       Usage: {usage}");

                                    continue;
                                }
                            case "list": {
                                    string clients = (from entry in clientNicknames select entry.Value).GetString('\n');
                                    clientMessageQueues[client].Enqueue($"The following clients are currently online:\n{clients}");

                                    continue;
                                }
                            case "uptime": {
                                    clientMessageQueues[client].Enqueue($"The current uptime is: {(DateTime.Now - ServerStart).Format()}");
                                    continue;
                                }
                            case "whisper":
                            case "w":
                            case "dm":
                            case "msg": {
                                    string receiver = parameters[0];
                                    string whisper = string.Join(" ", parameters.Skip(1));

                                    // ReSharper disable once SwitchStatementMissingSomeCases
                                    switch (parameters.Length) {
                                        case 0:
                                            // Check to see if they ran the command with no params, and tell them they need to specify a user
                                            clientMessageQueues[client].Enqueue("Please enter a user to send the message to.");
                                            continue;
                                        case 1:
                                            // Check to see if they only specified a user, and tell them they need to send a message
                                            clientMessageQueues[client].Enqueue($"Please enter a message to send to {receiver}.");
                                            continue;
                                    }

                                    // Check to see if they're sending a message to themselves.
                                    if (receiver == nick) {
                                        clientMessageQueues[client].Enqueue("Unfortunately you can't message yourself.");
                                        continue;
                                    }

                                    HandlePrivateMessage(nick, receiver, whisper);
                                    continue;
                                }
                            default:
                                clientMessageQueues[client].Enqueue($"Unknown command '/{command}'");
                                continue;
                        }
                    }

                    HandleNewMessage(new Message(nick, message.Trim()));
                } catch (Exception) {
                    connected = false;
                    client.Close();
                    client.Dispose();
                    clientNicknames.Remove(client);
                    SendMessageFromServer($"{nick} has disconnected");
                    ChatClientStateChanged(nick, ClientState.Disconnected);
                    ScreenShareClientStateChanged(client, ClientState.Disconnected);

                    Tuple<string, List<string>> temp = clientsGettingScreens.FirstOrDefault(who => who.Item1 == nick);
                    if (temp == null)
                        continue;

                    foreach (string receivingClient in temp.Item2) {
                        if (receivingClient != ServerNick && receivingClient != null)
                            clientMessageQueues[GetClientFromNick(receivingClient)].Enqueue(new Message(ServerCmd, $"DCFRMSCRN:{nick}"));
                    }

                    clientsGettingScreens.Remove(temp);
                }
            }

            connected = false;
        }

        private bool ValidateNick(BinaryWriter writer, ref string nick) {
            nick = nick.Trim();

            // Check to make sure nick isn't empty
            if (string.IsNullOrWhiteSpace(nick))
                return false;

            // Check to make sure the nick isn't taken
            if (clientNicknames.ContainsValue(nick)) {
                formatter.Serialize(writer.BaseStream, "That nickname is already taken!");
                return false;
            }

            // Make sure they aren't trying to impersonate the server.
            if (string.Equals(nick, ServerNick, StringComparison.CurrentCultureIgnoreCase) ||
                string.Equals(nick, ServerCmd, StringComparison.CurrentCultureIgnoreCase) ||
                string.Equals(nick, "admin", StringComparison.CurrentCultureIgnoreCase)) {
                formatter.Serialize(writer.BaseStream, "Nice try ;)");
                return false;
            }

            // Check to see if the nick is multi-word
            if (nick.Contains(' ')) {
                formatter.Serialize(writer.BaseStream, "Nicknames can only be one word!");
                return false;
            }

            // Check to see if there's any special characters in the nick
            if (new Regex("^[a-zA-Z0-9]+$").IsMatch(nick) == false) {
                formatter.Serialize(writer.BaseStream, "Nicknames can only contain letters and numbers!");
                return false;
            }

            return true;
        }

        private void ChangeNick(string newNick, BinaryWriter writer, TcpClient client) {
            string oldNick = clientNicknames[client];

            if (!ValidateNick(writer, ref newNick))
                return;

            clientNicknames[client] = newNick;
            clientMessageQueues[client].Enqueue($"Nickname successfully changed to: {newNick}");

            HandleNewMessage(new Message(ServerNick, $"{oldNick} is now known as {newNick}"));
            ChatClientStateChanged?.Invoke(newNick, ClientState.NameChange, oldNick);
            clientMessageQueues[client].Enqueue(new Message(ServerCmd, $"NICK:{newNick}"));
        }

        private TcpClient GetClientFromNick(string nick) {
            // Check to see if the target user is online.
            return clientNicknames.ContainsValue(nick)
                ? clientNicknames.First(clt => clt.Value == nick).Key
                : null;
        }

        private BinaryWriter GetWriterFromNick(string nick) {
            TcpClient client = GetClientFromNick(nick);
            return client == null ? null : new BinaryWriter(client.GetStream());
        }

        /// <summary>
        /// Send a message to everyone on the server.
        /// </summary>
        /// <param name="message">The message to send</param>
        private void HandleNewMessage(Message message) {
            if (string.IsNullOrWhiteSpace(message.Content))
                return;

            try {
                foreach (KeyValuePair<TcpClient, string> pair in clientNicknames)
                    clientMessageQueues[pair.Key].Enqueue(message);
            } catch (Exception ex) {
                ReceivedMessage?.Invoke(new Message("MSG::ERROR", ex.Message));
            }

            ReceivedMessage?.Invoke(message);
        }

        /// <summary>
        /// Send a message to the specified user, from the specified user.
        /// </summary>
        /// <param name="senderNick">The nickname of the user who sent the message</param>
        /// <param name="receiverNick">The nickname of the user who should receive the message</param>
        /// <param name="content">The message to be sent</param>
        private void HandlePrivateMessage(string senderNick, string receiverNick, string content) {
            // Get the TCPClient of the sender, it shouldn't be null, unless something else is very wrong.
            TcpClient sender = clientNicknames.FirstOrDefault(snd => snd.Value == senderNick).Key;
            if (sender == null) {
                ReceivedMessage?.Invoke(new Message("PM::ERROR", $"{senderNick} tried to send a private message, but was not found in the list of connected clients!"));
                return;
            }

            TcpClient receiverClient = GetClientFromNick(receiverNick);
            if (receiverClient == null) {
                // Send a message to the sender saying the person they're trying to message isn't connected.
                clientMessageQueues[sender].Enqueue(new Message(ServerNick, $"Couldn't find '{receiverNick}'! Make sure you spelled the username right, otherwise they probably aren't connected right now."));
                return;
            }

            PrivateMessage message = new PrivateMessage(senderNick, receiverNick, content);

            // Send the message to the recipient
            clientMessageQueues[receiverClient].Enqueue(message);

            // Send the message back to the sender so they can see that it sent
            clientMessageQueues[sender].Enqueue(message);

            // Log the message in the console 
            ReceivedMessage?.Invoke(new Message("PM", $"{senderNick} -> {receiverNick}: {content}"));
        }
    }
}
