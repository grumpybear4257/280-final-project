﻿using System;
using System.Net.Sockets;
using System.Windows;
using UtilLibrary;

namespace ChatServer {
    /// <summary>
    /// Interaction logic for ServerScreenViewWindow.xaml
    /// </summary>
    public partial class ServerScreenViewWindow : Window {
        private readonly TcpClient client;
        private readonly Server server;

        public ServerScreenViewWindow(Server server, TcpClient client) {
            InitializeComponent();

            this.server = server;
            this.client = client;

            server.ReceivedScreenShot += Server_ReceivedScreenShot;
        }

        private void Server_ReceivedScreenShot(TcpClient client, SharedImage sharedImage) {
            if (client != this.client)
                return;

            if (sharedImage.Image == null) {
                Close();
                return;
            }

            ClientScreenImg.Source = sharedImage.Image.ToSource();
            ClientScreenImg.InvalidateVisual();
        }

        private void ServerScreenViewWindow_OnClosed(object sender, EventArgs e) {
            server.ReceivedScreenShot -= Server_ReceivedScreenShot;
        }
    }
}
