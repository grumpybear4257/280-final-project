﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using UtilLibrary;

namespace ChatServer {
    /// <summary>
    /// Interaction logic for ServerWindow.xaml
    /// </summary>
    public partial class ServerWindow : Window {
        private static readonly string ServerName = Environment.MachineName;
        private readonly ConcurrentQueue<string> messageQueue = new ConcurrentQueue<string>();
        private readonly ConcurrentQueue<string> stateChangeQueue = new ConcurrentQueue<string>();
        private readonly ConcurrentQueue<Tuple<TcpClient, ClientState>> screenStateChangeQueue = new ConcurrentQueue<Tuple<TcpClient, ClientState>>();
        private readonly List<Tuple<TcpClient, ServerScreenViewWindow>> screenViewWindows = new List<Tuple<TcpClient, ServerScreenViewWindow>>();
        private readonly BackgroundWorker worker = new BackgroundWorker();
        private bool serverRunning;
        private Server server;

        private readonly List<IPAddress> addresses = Dns.GetHostEntry(ServerName).AddressList
            .Where(address => address.AddressFamily == AddressFamily.InterNetwork).ToList();

        public ServerWindow() {
            InitializeComponent();

            // Initialize Background Worker
            worker.WorkerReportsProgress = true;
            worker.ProgressChanged += Worker_ProgressChanged;
            worker.DoWork += Worker_DoWork;

            worker.RunWorkerAsync();

            foreach (IPAddress ipAddress in addresses)
                AddressCmbx.Items.Add(ipAddress);
        }

        private void Server_ReceivedMessage(Message message) {
            if (string.IsNullOrWhiteSpace(message.Content) == false)
                messageQueue.Enqueue($"{message.Sender}: {message.Content.Trim()}");
        }

        private void Server_ChatClientStateChanged(string nick, ClientState state, string oldNick = "") {
            switch (state) {
                case ClientState.Connected:
                    stateChangeQueue.Enqueue($"[ADD] {nick}");
                    break;
                case ClientState.Disconnected:
                    stateChangeQueue.Enqueue($"[REM] {nick}");
                    break;
                case ClientState.NameChange:
                    stateChangeQueue.Enqueue($"[ADD] {nick}");
                    stateChangeQueue.Enqueue($"[REM] {oldNick}");
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }
        }

        private void Server_ScreenShareClientStateChanged(TcpClient client, ClientState state) {
            screenStateChangeQueue.Enqueue(new Tuple<TcpClient, ClientState>(client, state));
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e) {
            while (worker.CancellationPending == false) {
                worker.ReportProgress(0);
                Thread.Sleep(100);
            }
        }

        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            // Dump out if server's not running, since we don't have anything to update then.
            if (server == null) {
                return;
            }

            // Check to see if we need to open a window to show a client's screen.
            while (screenStateChangeQueue.IsEmpty == false) {
                if (screenStateChangeQueue.TryDequeue(out Tuple<TcpClient, ClientState> test) == false)
                    return;

                TcpClient client = test.Item1;
                ClientState state = test.Item2;

                if (state == ClientState.Connected) {
                    // Open a window
                    ServerScreenViewWindow serverScreenView = new ServerScreenViewWindow(server, client);
                    serverScreenView.Show();

                    screenViewWindows.Add(new Tuple<TcpClient, ServerScreenViewWindow>(client, serverScreenView));
                } else if (state == ClientState.Disconnected) {
                    // Close the window
                    screenViewWindows.FirstOrDefault(temp => temp.Item1 == client)?.Item2.Close();
                }
            }

            // Update the UI
            UptimeLbl.Content = $"Uptime: {(DateTime.Now - server.ServerStart).Format()}";

            while (messageQueue.IsEmpty == false) {
                if (messageQueue.TryDequeue(out string message))
                    MessagesLsbx.Items.Add(message);
            }

            while (stateChangeQueue.IsEmpty == false) {
                if (!stateChangeQueue.TryDequeue(out string stateChange))
                    continue;

                string[] temp = stateChange.Split(' ');
                string change = temp.First();
                string value = temp.Last();

                switch (change) {
                    case "[ADD]":
                        if (value == Server.ServerNick) {
                            // Server just started
                            ClientsLsbx.Items.Clear();

                            break;
                        }

                        ClientsLsbx.Items.Add(value);
                        break;
                    case "[REM]":
                        if (value == Server.ServerNick) {
                            // Server just stopped
                            ClientsLsbx.Items.Clear();

                            break;
                        }

                        ClientsLsbx.Items.Remove(value);
                        break;
                }
            }
        }

        private void StarServerBtn_OnClick(object sender, RoutedEventArgs e) {
            if (serverRunning) {
                StarServerBtn.Content = "Start Server";
                UptimeLbl.Visibility = Visibility.Hidden;
                serverRunning = false;
                server.Stop();
                return;
            }

            if (int.TryParse(PortTxt.Text, out int port) == false) {
                MessagesLsbx.Items.Add("Please enter a valid port!");
                return;
            }

            if (!(AddressCmbx.SelectedValue is IPAddress address))
                return;

            TcpListener screenListener = new TcpListener(address, port + 1);
            TcpListener chatListener = new TcpListener(address, port);
            screenListener.Start();
            chatListener.Start();

            server = new Server(screenListener, chatListener);
            server.ReceivedMessage += Server_ReceivedMessage;
            server.ChatClientStateChanged += Server_ChatClientStateChanged;
            server.ScreenShareClientStateChanged += Server_ScreenShareClientStateChanged;

            server.Start();
            serverRunning = true;

            MessagesLsbx.Items.Clear();
            StarServerBtn.Content = "Stop Server";
            UptimeLbl.Visibility = Visibility.Visible;

            MessagesLsbx.Items.Add($"Server listening on {address}:{port}");
        }

        private void PortTxt_OnPreviewTextInput(object sender, TextCompositionEventArgs e) {
            if (int.TryParse(e.Text, out int _) == false)
                e.Handled = true;
        }

        private void PortTxt_OnPasting(object sender, DataObjectPastingEventArgs e) {
            if (e.DataObject.GetDataPresent(typeof(string))) {
                string text = e.DataObject.GetData(typeof(string)) as string;
                if (int.TryParse(text, out int _) == false)
                    e.CancelCommand();
            } else {
                e.CancelCommand();
            }
        }

        private void SendBtn_OnClick(object sender, RoutedEventArgs e) {
            if (serverRunning == false) {
                MessagesLsbx.Items.Add("Server isn't running!");
                return;
            }

            try {
                string input = MessageTxt.Text.Trim();
                server.SendMessageFromServer(input);
            } catch (NullReferenceException) {
                MessagesLsbx.Items.Add("Server isn't running!");
            } finally {
                MessageTxt.Clear();
            }
        }

        private void ViewScreenBtn_OnClick(object sender, RoutedEventArgs e) {
            if (ClientsLsbx.SelectedItem == null)
                return;

            string nick = ClientsLsbx.SelectedItem.ToString();
            server.ViewScreen(nick);
        }
    }
}
